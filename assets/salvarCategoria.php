<?php
include_once './config/config.php';
include_once './class/class.categoria.php';

$ca = new Categoria();

//FAÇO VALIDACOES NOS FILTROS
$dados['nome'] = filter_var($_POST['category_name'], FILTER_SANITIZE_STRING);
$dados['codigo'] = filter_var($_POST['category_code'], FILTER_SANITIZE_STRING);

//CHAMO O METODO PARA SALVAR
$ca->cadastrarCategoria($dados);

echo "<script>
        alert('Category successfully saved.'); 
        window.location.href = 'categories.php';
	</script>";
die;