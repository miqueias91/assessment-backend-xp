<?php
include_once './config/config.php';
include_once './class/class.produto.php';

$pr = new Produto();

//FAÇO VALIDACOES NO FILTRO
$idProduct = filter_var($_GET['idProduct'], FILTER_SANITIZE_NUMBER_INT);

//CHAMO O METODO PARA EXCLUIR
$pr->excluirProduto($idProduct);