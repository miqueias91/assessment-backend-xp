-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 28-Jan-2020 às 21:22
-- Versão do servidor: 5.7.28-0ubuntu0.16.04.2
-- PHP Version: 7.1.33-4+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_webjump`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `codigo` varchar(256) NOT NULL,
  `nome` varchar(256) NOT NULL,
  `status` enum('ativo','inativo') NOT NULL DEFAULT 'ativo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `idproduto` int(11) NOT NULL,
  `nome` varchar(256) NOT NULL,
  `sku` varchar(256) NOT NULL,
  `preco` float(9,2) NOT NULL DEFAULT '0.00',
  `descricao` text,
  `quantidade` float(8,3) NOT NULL DEFAULT '0.00',
  `imagem` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estrutura da tabela `produto_categoria`
--

CREATE TABLE `produto_categoria` (
  `idproduto_categoria` int(11) NOT NULL,
  `produto` int(11) NOT NULL,
  `categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `produto_categoria`
--
ALTER TABLE `produto_categoria` ADD PRIMARY KEY(`idproduto_categoria`);
ALTER TABLE `produto_categoria` CHANGE `idproduto_categoria` `idproduto_categoria` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `produto_categoria`
  ADD KEY `produto` (`produto`),
  ADD KEY `categoria` (`categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `idproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produto_categoria`
--
ALTER TABLE `produto_categoria`
  ADD CONSTRAINT `produto_categoria_ibfk_1` FOREIGN KEY (`produto`) REFERENCES `produto` (`idproduto`) ON UPDATE CASCADE,
  ADD CONSTRAINT `produto_categoria_ibfk_2` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`idcategoria`) ON UPDATE CASCADE;

ALTER TABLE `produto_categoria` DROP FOREIGN KEY `produto_categoria_ibfk_1`; ALTER TABLE `produto_categoria` ADD CONSTRAINT `produto_categoria_ibfk_1` FOREIGN KEY (`produto`) REFERENCES `produto`(`idproduto`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `produto_categoria` DROP FOREIGN KEY `produto_categoria_ibfk_2`; ALTER TABLE `produto_categoria` ADD CONSTRAINT `produto_categoria_ibfk_2` FOREIGN KEY (`categoria`) REFERENCES `categoria`(`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;