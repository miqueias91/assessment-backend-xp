<?php
include_once './config/config.php';
include_once './class/class.produto.php';

$pr = new Produto();

//FAÇO VALIDACOES NOS FILTROS
$dados['nome'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
$dados['sku'] = filter_var($_POST['sku'], FILTER_SANITIZE_STRING);
$dados['preco'] = str_replace(',','.',filter_var($_POST['price'], FILTER_SANITIZE_STRING));
$dados['descricao'] = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
$dados['quantidade'] = filter_var($_POST['quantity'], FILTER_SANITIZE_STRING);
$dados['categoria'] = filter_var_array($_POST['category'], FILTER_SANITIZE_STRING);

if($file['size'] > 0){
    //VERIFICO O NOME
    $extensaotmp = explode(".",filter_var($file['name'], FILTER_SANITIZE_STRING));
    //FAÇO CODIGICAÇÕES NO NOME DA IMAGEM
    $extensao = $extensaotmp[sizeof($extensaotmp)-1];
    $arquivo = filter_var($file['tmp_name'], FILTER_SANITIZE_STRING);
    $caminho_imagem =  base64_encode($arquivo.date('YmdHmi').$extensao).'.'.$extensao;
    //SALVO NA PASTA
    copy($arquivo, "images/product/$caminho_imagem");
    $caminho_imagem = "images/product/$caminho_imagem";
}
else{
    $caminho_imagem = 'images/no-image.png';
}  
$dados['imagem'] = filter_var($caminho_imagem, FILTER_SANITIZE_STRING);
$dados['quantidade'] = filter_var($_POST['quantity'], FILTER_SANITIZE_STRING);

//CHAMO O METODO PARA SALVAR
$pr->cadastrarProduto($dados);

echo "<script>
        alert('Product successfully saved.'); 
        window.location.href = 'products.php';
	</script>";
die;