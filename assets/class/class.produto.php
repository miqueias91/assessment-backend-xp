<?php
	class Produto extends Conexao {
		public static $instance;

		public function __construct(){}

		public static function getInstance() {
	        self::$instance = new Produto();
	        return self::$instance;
	    }
	    //CADASTRA produto
		public function cadastrarProduto($dados){
			try {
	            $sql = "INSERT INTO produto (
	                idproduto, 
					nome,
					sku,
					preco,
					descricao,
					quantidade,
					imagem
					)
					VALUES (
	                :idproduto, 
					:nome,
					:sku,
					:preco,
					:descricao,
					:quantidade,
					:imagem
					)

	                ";
	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(":idproduto", null, PDO::PARAM_INT);
	            $pdo->bindValue(":nome", $dados['nome'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":sku", $dados['sku'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":preco", $dados['preco'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":descricao", $dados['descricao'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":quantidade", $dados['quantidade'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":imagem", $dados['imagem'], PDO::PARAM_STR);	            
	            $pdo->execute();

	            $idproduto = Conexao::ultimoID();
	            //VERIFICO SE TEM CATEGORIA
	            if ($dados['categoria'][0]) {
	            	foreach ($dados['categoria'] as $categoria) {
	            		//CADASTRA A CATEGORIA DO PRODUTO
	            		$this->cadastrarCategoriaProduto($idproduto, $categoria);
	            	}
	            }

	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		//CADASTRA A categoria do produto
		public function cadastrarCategoriaProduto($produto, $categoria){
			try {
	            $sql = "INSERT INTO produto_categoria (
	                idproduto_categoria, 
					produto,
					categoria
					)
					VALUES (
	                :idproduto_categoria, 
					:produto,
					:categoria
					)

	                ";
	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(":idproduto_categoria", null, PDO::PARAM_INT);
	            $pdo->bindValue(":produto", $produto, PDO::PARAM_STR);	            
	            $pdo->bindValue(":categoria", $categoria, PDO::PARAM_STR);	            
	            $pdo->execute();
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function buscarCategoriaProduto($idproduto = null){
			$filtro = "";
			$filtro .= isset($idproduto) ? " AND pc.produto = :idproduto" : "";

			try {
	            $sql = "SELECT
	            	ca.nome AS 'nomecategoria',
	            	pc.categoria AS 'categoria'

	                FROM produto_categoria pc
	                INNER JOIN produto pr ON pr.idproduto = pc.produto
	                INNER JOIN categoria ca ON ca.idcategoria = pc.categoria                        
	                WHERE pc.idproduto_categoria > :idprodutocategoria
	                $filtro
	                group by pc.idproduto_categoria
					ORDER BY ca.nome
	            ";

	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(':idprodutocategoria', 0, PDO::PARAM_INT);
	            if ($idproduto) {
		            $pdo->bindValue(':idproduto', $idproduto, PDO::PARAM_INT);
	            }
	         
	            $pdo->execute();
	            return $pdo->fetchAll(PDO::FETCH_BOTH);
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function buscarProduto($idproduto = null, $sku = null, $nome = null){
			$filtro = "";
			$filtro .= isset($idproduto) ? " AND idproduto = :idproduto" : "";
			$filtro .= isset($sku) ? " AND sku = :sku" : "";
			$filtro .= isset($nome) ? " AND nome = :nome" : "";			

			try {
	            $sql = "SELECT *

	                FROM produto	                             
	                WHERE idproduto > :id_produto
	                $filtro
	                group by idproduto
					ORDER BY idproduto DESC 
	            ";

	            $pdo = Conexao::getInstance()->prepare($sql);
				
	            $pdo->bindValue(':id_produto', 0, PDO::PARAM_INT);
	            
	            if ($idproduto) {
		            $pdo->bindValue(':idproduto', $idproduto, PDO::PARAM_INT);
	            }     
	            if ($sku) {
		            $pdo->bindValue(':sku', $sku, PDO::PARAM_STR);
	            }
	           	if ($nome) {
		            $pdo->bindValue(':nome', $nome, PDO::PARAM_STR);
	            }
	         
	            $pdo->execute();
	            return $pdo->fetchAll(PDO::FETCH_BOTH);
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function excluirImagemPastaProduto($idproduto){
			try {
	            $sql = "SELECT 
	            		imagem

	                FROM produto	                             
	                WHERE idproduto = :idproduto
	            ";
	            $pdo = Conexao::getInstance()->prepare($sql);
		        $pdo->bindValue(':idproduto', $idproduto, PDO::PARAM_INT);
	            $pdo->execute();
	            $imagem = $pdo->fetchAll(PDO::FETCH_BOTH);
	            if($imagem){
	            	unlink($imagem[0]['imagem']);
	            }
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function alterarProduto($dados){
			$this->excluirImagemPastaProduto($dados['idproduto']);
			$this->excluirCategoriaProduto($dados['idproduto']);
			try {
	            $sql = "UPDATE produto
	            	SET
						nome 		= :nome,
						sku 		= :sku,
						preco 		= :preco,
						descricao 	= :descricao,
						quantidade 	= :quantidade,
						imagem 		= :imagem

					WHERE idproduto = :idproduto
	                ";
	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(":idproduto", $dados['idproduto'], PDO::PARAM_INT);
	            $pdo->bindValue(":nome", $dados['nome'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":sku", $dados['sku'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":preco", $dados['preco'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":descricao", $dados['descricao'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":quantidade", $dados['quantidade'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":imagem", $dados['imagem'], PDO::PARAM_STR);	
	            $pdo->execute();

	            foreach ($dados['categoria'] as $categoria) {
	            	//CADASTRA A CATEGORIA DO PRODUTO
	            	$this->cadastrarCategoriaProduto($dados['idproduto'], $categoria);
	            }
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function excluirCategoriaProduto($idproduto) {
	        try {
		            $sql = "DELETE 
		            		FROM produto_categoria 
		            		WHERE produto = :produto";
		            $pdo = Conexao::getInstance()->prepare($sql);
			        $pdo->bindValue(':produto', $idproduto, PDO::PARAM_INT);
			        $pdo->execute();
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
	    }

		public function excluirProduto($idproduto) {
	        try {
		            $sql = "DELETE 
		            		FROM produto 
		            		WHERE idproduto = :idproduto";
		            $pdo = Conexao::getInstance()->prepare($sql);
			        $pdo->bindValue(':idproduto', $idproduto, PDO::PARAM_INT);
			        $pdo->execute();
			       	$this->excluirImagemPastaProduto($idproduto);
			       	echo "<script>alert('Product successfully deleted.'); window.location.href = './products.php';</script>";

	        } 
	        catch (Exception $e) {
	        	echo "<script>alert('Could not delete product.'); window.location.href = './products.php';</script>";

	            echo "<br>".$e->getMessage();
	        }
	    }

		public function buscarTotalProduto(){
			$filtro = "";
			try {
	            $sql = "SELECT idproduto

	                FROM produto	                             
	                WHERE idproduto > :id_produto
	                $filtro
	                group by idproduto
					ORDER BY idproduto, nome, sku
	            ";

	            $pdo = Conexao::getInstance()->prepare($sql);
				
	            $pdo->bindValue(':id_produto', 0, PDO::PARAM_INT);	         
	            $pdo->execute();
	            return count($pdo->fetchAll(PDO::FETCH_BOTH));
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}















	}