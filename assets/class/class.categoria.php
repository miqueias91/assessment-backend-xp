<?php
	class Categoria extends Conexao {
		public static $instance;

		public function __construct(){}

		public static function getInstance() {
	        self::$instance = new Categoria();
	        return self::$instance;
	    }
	    //CADASTRA A CATEGORIA
		public function cadastrarCategoria($dados){
			try {
	            $sql = "INSERT INTO categoria (
	                idcategoria, 
					codigo,
					nome
					)
					VALUES (
	                :idcategoria, 
					:codigo,
					:nome
					)

	                ";
	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(":idcategoria", null, PDO::PARAM_INT);
	            $pdo->bindValue(":codigo", $dados['codigo'], PDO::PARAM_STR);	            
	            $pdo->bindValue(":nome", $dados['nome'], PDO::PARAM_STR);	            
	            $pdo->execute();
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function buscarCategoria($idcategoria = null, $codigo = null, $nome = null){
			$filtro = "";
			$filtro .= isset($idcategoria) ? " AND idcategoria = :idcategoria" : "";
			$filtro .= isset($codigo) ? " AND codigo = :codigo" : "";
			$filtro .= isset($nome) ? " AND nome = :nome" : "";			

			try {
	            $sql = "SELECT *

	                FROM categoria	                             
	                WHERE idcategoria > :id_categoria
	                $filtro
	                group by idcategoria
					ORDER BY idcategoria, nome, codigo
	            ";

	            $pdo = Conexao::getInstance()->prepare($sql);
				
	            $pdo->bindValue(':id_categoria', 0, PDO::PARAM_INT);
	            
	            if ($idcategoria) {
		            $pdo->bindValue(':idcategoria', $idcategoria, PDO::PARAM_INT);
	            }     
	            if ($codigo) {
		            $pdo->bindValue(':codigo', $codigo, PDO::PARAM_STR);
	            }
	           	if ($nome) {
		            $pdo->bindValue(':nome', $nome, PDO::PARAM_STR);
	            }
	         
	            $pdo->execute();
	            return $pdo->fetchAll(PDO::FETCH_BOTH);
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function alterarCategoria($dados){
			try {
	            $sql = "UPDATE categoria
	            	SET
						codigo 	= :codigo,
						nome 	= :nome

					WHERE idcategoria = :idcategoria
	                ";
	            $pdo = Conexao::getInstance()->prepare($sql);
	            $pdo->bindValue(":idcategoria", $dados['idcategoria'], PDO::PARAM_INT);
	            $pdo->bindValue(":codigo", $dados['codigo'], PDO::PARAM_STR);
	            $pdo->bindValue(":nome", $dados['nome'], PDO::PARAM_STR);
	            $pdo->execute();
	        } 
	        catch (Exception $e) {
	            echo "<br>".$e->getMessage();
	        }
		}

		public function excluirCategoria($idcategoria) {
	        try {
		            $sql = "DELETE 
		            		FROM categoria 
		            		WHERE idcategoria = :idcategoria";
		            $pdo = Conexao::getInstance()->prepare($sql);
			        $pdo->bindValue(':idcategoria', $idcategoria, PDO::PARAM_INT);
			        $pdo->execute();
			       	echo "<script>alert('Category successfully deleted.'); window.location.href = './categories.php';</script>";

	        } 
	        catch (Exception $e) {
	        	echo "<script>alert('Could not delete category.'); window.location.href = './categories.php';</script>";

	            echo "<br>".$e->getMessage();
	        }
	    }
















	}