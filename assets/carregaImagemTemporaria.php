<?php
    include_once("./config/config.php");
    //SE FOR MAIOR QUE ZERO, SIGINIFICA QUE CARREGOU UMA IMAGEM VALIDA
    if($file['size'] > 0){
        //VERIFICO O NOME
        $extensaotmp = explode(".",filter_var($file['name'], FILTER_SANITIZE_STRING));
        //FAÇO CODIGICAÇÕES NO NOME DA IMAGEM
        $extensao = $extensaotmp[sizeof($extensaotmp)-1];
        $arquivo = filter_var($file['tmp_name'], FILTER_SANITIZE_STRING);
        $caminho_imagem =  base64_encode($arquivo.date('YmdHmi').$extensao).'.'.$extensao;
        //SALVO NA PASTA
        copy($arquivo, "images/tmp/$caminho_imagem");
        $caminho_imagem = "images/tmp/$caminho_imagem";
    }
    else{
        $caminho_imagem = null;
    }   
    echo $caminho_imagem;
         
