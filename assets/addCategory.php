<?php
include_once './config/config.php';
include_once './class/class.categoria.php';

$codigo = null;
$nome = null;
if ($idcategory) {
  $ca = new Categoria();
  //BUSCO A CATEGORIA
  $categorias = $ca->buscarCategoria($idcategory);
  $dados = $categorias[0];
  $codigo = $dados['codigo'];
  $nome = $dados['nome'];
}
?>
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | <?=$idcategory ? 'Edit' : 'New'?> Category</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.html"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.html" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item"><?=$idcategory ? 'Edit' : 'New'?> Category</h1>
    
    <form id="form" method="POST" action="#">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="category_name" class="input-text" value="<?=$nome?>" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="category_code" class="input-text" value="<?=$codigo?>" />
        
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Back</a>
        <input class="btn-submit btn-action" id="<?=$idcategory ? 'salvar_edicao' : 'salvar_categoria'?>"  type="button" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
<!-- Footer -->
<script src="js/jquery-3.4.1.min.js"></script>

<script>
  $( document ).ready(function() {
    $( "#salvar_categoria" ).click(function() {
      //VERIFICO SE PREENCHEU O NOME DA CATEGORIA
      if ($('#category-name').val() == '') {
        alert("Attention, inform the name of the category!");
      }
      //VERIFICO SE PREENCHEU O CODIGO DA CATEGORIA
      else if ($('#category-Code').val() == '') {
        alert("Attention, enter the category code!");
      }
      //SE ESTIVER TUDO CERTO, ENVIO PARA O ARQUIVO DE INSERCAO
      else{
        if (confirm("Do you really want to save?")) {
          $("#form").attr("action","salvarCategoria.php");
          $("#form").submit();
        }
      }
    });

    $( "#salvar_edicao" ).click(function() {
      //VERIFICO SE PREENCHEU O NOME DA CATEGORIA
      if ($('#category-name').val() == '') {
        alert("Attention, inform the name of the category!");
      }
      //VERIFICO SE PREENCHEU O CODIGO DA CATEGORIA
      else if ($('#category-Code').val() == '') {
        alert("Attention, enter the category code!");
      }
      //SE ESTIVER TUDO CERTO, ENVIO PARA O ARQUIVO DE INSERCAO
      else{
        if (confirm("Do you really want to save?")) {
          $("#form").attr("action","editarCategoria.php?idcategory=<?=$idcategory?>");
          $("#form").submit();
        }
      }
    });
  });
</script>
</body>
</html>
