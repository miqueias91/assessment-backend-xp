<?php
include_once './config/config.php';
include_once './class/class.categoria.php';

$ca = new Categoria();

//FAÇO VALIDACOES NO FILTRO
$idcategory = filter_var($_GET['idcategory'], FILTER_SANITIZE_NUMBER_INT);

//CHAMO O METODO PARA EXCLUIR
$ca->excluirCategoria($idcategory);