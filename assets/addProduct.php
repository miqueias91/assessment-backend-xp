<?php
include_once './config/config.php';
include_once './class/class.categoria.php';
include_once './class/class.produto.php';

$ca = new Categoria();
//BUSCO AS CATEGORIAS
$categorias = $ca->buscarCategoria();

$option_category = "";
$sku = null;
$nome = null;
$preco = null;
$descricao = null;
$quantidade = null;
$imagem = 'images/no-image.png';

if ($idProduct) {
  $pr = new Produto();
  //BUSCO OS PRODUTOS
  $produtos = $pr->buscarProduto($idProduct);
  $dados = $produtos[0];
  $sku = $dados['sku'];
  $nome = $dados['nome'];
  $preco = str_replace('.', ',', $dados['preco']);
  $descricao = $dados['descricao'];
  $quantidade = $dados['quantidade'];
  $imagem = $dados['imagem'];

  //BUSCO AS CATEGORIAS DO PRODUTO E DEIXO SELECIONADO
  $categoriasProduto = $pr->buscarCategoriaProduto($idProduct);


  if ($categorias) {
    //SE A EXISTIR CATEGORIA, PERCORRO E CRIO O OPTION
    foreach ($categorias as $value) {
      $selected = '';
      if ($categoriasProduto) {
        foreach ($categoriasProduto as $categoria) {
          //VERIFICO SE O OPTION É IGUAL A CATEGORIA DO PRODUTO
          if ($categoria['categoria'] == $value['idcategoria']) {
            $selected = "selected = 'selected'";
          }
        }
      }
      $option_category .= "<option {$selected} value='$value[idcategoria]'>$value[nome]</option>\n";
    }
  }
  else{
    $option_category = "<option value=''>No categories found</option>";
  }
}
else{
  if ($categorias) {
    //SE A EXISTIR CATEGORIA, PERCORRO E CRIO O OPTION
    foreach ($categorias as $value) {
      $option_category .= "<option value='$value[idcategoria]'>$value[nome]</option>\n";
    }
  }
  else{
    $option_category = "<option value=''>No categories found</option>";
  }
}
?>
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | <?=$idProduct ? 'Edit' : 'New'?> Product</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item"><?=$idProduct ? 'Edit' : 'New'?> Product</h1>
    
    <form id="form" method="POST" action="#" enctype='multipart/form-data' name="form">
      <input type="hidden" name="imagem_anterior" value="<?=$imagem?>">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" name="sku" id="sku" class="input-text" value="<?=$sku?>" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" name="name" id="name" class="input-text" value="<?=$nome?>" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" name="price" id="price" class="input-text valor" value="<?=$preco?>" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" name="quantity" id="quantity" class="input-text quantidade" value="<?=$quantidade?>" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple name="category[]" id="category" class="input-text">
          <?=$option_category?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea name="description" id="description" class="input-text"><?=$descricao?></textarea>
      </div>
      <div class="input-field">
        <label for="" class="label">Product Image</label>
        <img id="image" src="<?=$imagem?>" style="margin: 0 auto;" layout="responsive" width="164" height="145" alt="No image" />
        <input style="width: 117px;margin: 10px auto;display: block;" class="image" type="file" name="file" class="input-text"> 

      </div>
      <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input class="btn-submit btn-action" id="<?=$idProduct ? 'salvar_edicao' : 'save_product'?>"  type="button" value="Save Product" />

      </div>
      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
<!-- Footer -->
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/maskMoney.js"></script>

<script>
  $( document ).ready(function() {
      //MASCARAS DOS CAMPOS
    $('.valor').maskMoney({
      allowNegative: true,
      thousands: '',
      decimal: ',',
      affixesStay: false
    });
    $('.quantidade').maskMoney({
      thousands:'',
      decimal:'.',
      precision: 3,
      affixesStay: false
    });
    $( "#save_product" ).click(function() {
      //VERIFICO SE PREENCHEU
      if ($('#sku').val() == '') {
        alert("Attention, inform the Product SKU!");
      }
      else if ($('#name').val() == '') {
        alert("Attention, inform the name of the product!");
      }
      else if ($('#price').val() == '') {
        alert("Attention, inform the price!");
      }
      else if ($('#quantity').val() == '') {
        alert("Attention, inform the quantity!");
      }
      else if ($('#category').val() == '') {
        alert("Attention, inform the category!");
      }
      else if ($('#description').val() == '') {
        alert("Attention, inform the description!");
      }
      //SE ESTIVER TUDO CERTO, ENVIO PARA O ARQUIVO DE INSERCAO
      else{
        if (confirm("Do you really want to save?")) {
          $("#form").attr("action","salvarProduto.php");
          $("#form").submit();
        }
      }
    });
    $( "#salvar_edicao" ).click(function() {
      //VERIFICO SE PREENCHEU
      if ($('#sku').val() == '') {
        alert("Attention, inform the Product SKU!");
      }
      else if ($('#name').val() == '') {
        alert("Attention, inform the name of the product!");
      }
      else if ($('#price').val() == '') {
        alert("Attention, inform the price!");
      }
      else if ($('#quantity').val() == '') {
        alert("Attention, inform the quantity!");
      }
      else if ($('#category').val() == '') {
        alert("Attention, inform the category!");
      }
      else if ($('#description').val() == '') {
        alert("Attention, inform the description!");
      }
      //SE ESTIVER TUDO CERTO, ENVIO PARA O ARQUIVO DE INSERCAO
      else{
        if (confirm("Do you really want to save?")) {
          $("#form").attr("action","editarProduto.php?idProduct=<?=$idProduct?>");
          $("#form").submit();
        }
      }
    });


    $('.image').change(function () {
      //QUEBRO O NOME DA FOTO, PARA PEGAR A EXTENSÃO
      var result = $(this).val().split('.');
      //ACEITO CARREGAR APENAS ALGUNS FORMATOS
      if(result[1] == "jpg" || result[1] == "jpeg" || result[1] == "png"){
        //PEGO OS DADOS DO FORME
        var formdata = new FormData($("form[name='form']")[0]);
        $.ajax({
          type: 'POST',
          url: "./carregaImagemTemporaria.php",
          data: formdata,
          processData: false,
          contentType: false,
        }).done(function (data) {
          $('#image').attr('src',data);
        });          
      }
      else{
        alert('Attention, select images of jpg, jpeg or png format!');
      }
    }); 














  });
</script>
</body>
</html>
